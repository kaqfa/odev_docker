## Odoo Development Environment

Repository ini dimaksudkan untuk memudahkan developer odoo untuk setup development environment di PC pribadinya terserah apapun sistem operasinya dengan mudah.

### Langkah Persiapan

* Install Docker sesuai OS anda:
  - jika Windows 10 maka download [Docker for Windows](https://store.docker.com/editions/community/docker-ce-desktop-windows)
  - jika Windows 7 maka download [Docker-Toolbox](https://download.docker.com/win/stable/DockerToolbox.exe)
* Clone project ini ke folder manapun yang anda mau
* Jalankan Docker / Docker Quickstart
* Pada command prompt / terminal masuk ke dalam direktori hasil clone repositori ini
* Pastikan terdapat file **docker-compose.yml** pada direktori tersebut
* Jalankan perintah `docker-compose up -d` dan tunggu hingga docker selesai melakukan download dan setup environment.
* Setelah environtment web dan db siap, buka browser dan buka URL `localhost:8069`
* Buat database (nama database wajib menggunakan prefix `odoo_`) dan admin (inputannya tertulis "Email" tapi sebenarnya adalah username untuk user superadmin) untuk instalasi odoo baru, lalu klik **Create Database**
* Tunggu beberapa saat hingga halaman login muncul dan login-lah dengan menggunakan email & password yang telah dibuat sebelumnya.
  

### Langkah Add-On Development

* Salin direktori **addon_template** ke dalam direktori **dev-addons**
* Rename direktori yang telah disalin dengan nama addon yang akan kita buat
* Buka file `__manifest__.py` dan modifikasi isinya sesuai identitas addon akan kita buat
<!-- * Masuk ke dalam terminal / command prompt dan jalankan perintah `docker restart odev_docker_web_1` -->
* Buka browser masuk ke odoo sebagai admin, kemudian masuk ke menu *Settings*
* Pada menu **Setting > Dashboard** klik link **Activate the developer mode**
* Setelah debug mode aktif, masuk ke menu **Apps**
* Pilih menu **Update Apps List**, kemudian cek apakah addon baru kita sudah muncul